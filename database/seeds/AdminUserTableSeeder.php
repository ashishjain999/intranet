<?php use Illuminate\Database\Seeder;

class AdminUserTableSeeder extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name'        => 'Admin User',
            'email'       => 'admin@intranet.com',
            'password'    => bcrypt('ashish'),
            'user_status' => 1, //User is enabled
            'created_at'  => date('Y-m-d H:i:s'),
            'updated_at'  => date('Y-m-d H:i:s'),
        ]);
    }
}
