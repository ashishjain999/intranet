<?php namespace App\Classes\Posts;

use App\Repositories\Contract\CommentsLikesInterface;

/**
 * Class CommentsLikesProvider
 *
 * @package App\Classes\Posts
 */
class CommentsLikesProvider
{

    /**
     * @var CommentsLikesInterface
     */
    private $commentLike;

    /**
     * CommentsLikesProvider constructor.
     *
     * @param CommentsLikesInterface $commentLike
     */
    public function __construct(CommentsLikesInterface $commentLike)
    {
        $this->commentLike = $commentLike;
    }

    /**
     * Add commentsLikes to comments
     *
     * @param array $users
     * @param array $comments
     * @param array $commentId
     * @return array
     */
    public function getAllCommentsLikes(Array $users, Array $comments, Array $commentId)
    {
        $commentLikesRepo = $this->commentLike->getAllCommentsLikes($commentId);
        foreach ($commentLikesRepo as $commentLikes) {
            $comments[ $commentLikes->comment_id ]->total_comments_likes[] = [
                'user_name'  => $users[ $commentLikes->user_id ]->name,
                'comment_id' => $commentLikes->comment_id,
                'user_id'    => $commentLikes->user_id,
                'user_photo' => ($users[ $commentLikes->user_id ]->file == null) ? '/images/default-grav.jpg' : $users[ $commentLikes->user_id ]->file,
            ];
        }

        return $comments;
    }

    /**
     * Get comment by userId & postId
     *
     * @param $userId
     * @param $commentId
     * @return mixed
     */
    public function getCommentsOnUserAndPost($userId, $commentId)
    {
        return $this->commentLike->getCommentsOnUserAndPost($userId, $commentId);
    }

    /**
     * Delete an existing like
     *
     * @param $id
     * @return mixed
     */
    public function deleteCommentLike($id)
    {
        return $this->commentLike->deleteLike($id);
    }

    /**
     * Get count of likes for particular comment
     *
     * @param $commentId
     * @return mixed
     */
    public function getLikeCount($commentId)
    {
        return $this->commentLike->getLikeCount($commentId);
    }

    /**
     * Get all users who liked the comments
     *
     * @param $commentId
     * @return mixed
     */
    public function getCommentsUsers($commentId)
    {
        return $this->commentLike->getCommentsUsers($commentId);
    }

    /**
     * Save comment to a database
     *
     * @param $data
     * @return array
     */
    public function saveCommentLike($data)
    {
        return $this->commentLike->saveCommentLike($data);
    }


    /**
     * Unused method
     *
     * @param array $users
     * @param array $comments
     * @param array $commentId
     */
    public function getAllCommentsLikesForAll(Array $users, Array $comments, Array $commentId)
    {
        $commentLikesRepo = $this->commentLike->getAllCommentsLikes($commentId);
        dd($commentLikesRepo);
    }
}