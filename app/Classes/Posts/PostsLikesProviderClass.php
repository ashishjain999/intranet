<?php namespace App\Classes\Posts;

use App\Repositories\Contract\LikesInterface;

/**
 * Class PostsLikesProviderClass
 *
 * @package App\Classes\Posts
 */
class PostsLikesProviderClass
{

    /**
     * @var LikesInterface
     */
    private $likes;

    /**
     * PostsLikesProviderClass constructor.
     *
     * @param LikesInterface $likes
     */
    public function __construct(LikesInterface $likes)
    {
        $this->likes = $likes;
    }

    /**
     * Get all postLikes of a single post
     *
     * @param       $postsRepo
     * @param array $users
     * @param array $id
     * @return mixed
     */
    public function getLikesForSinglePost($postsRepo, Array $users, Array $id)
    {
        $likesRepo = $this->likes->getAllLikes($id);
        foreach ($likesRepo as $like) {
            $postsRepo->total_likes[] = [
                'user_name'  => $users[ $like->user_id ]->name,
                'post_id'    => $like->post_id,
                'user_id'    => $like->user_id,
                'user_photo' => ($users[ $like->user_id ]->file == null) ? '/images/default-grav.jpg' : $users[ $like->user_id ]->file,
            ];
        }

        return $postsRepo;
    }

    /**
     * Get all postLikes of all post
     *
     * @param       $postsRepo
     * @param array $users
     * @param array $id
     * @return mixed
     */
    public function getLikesFromAllPosts($postsRepo, Array $users, Array $id)
    {
        $likesRepo = $this->likes->getAllLikes($id);

        foreach ($likesRepo as $like) {
            $postsRepo[ 'data' ][ $like->post_id ]->total_likes[] = [
                'user_name'  => $users[ $like->user_id ]->name,
                'post_id'    => $like->post_id,
                'user_id'    => $like->user_id,
                'user_photo' => ($users[ $like->user_id ]->file == null) ?
                    '/images/default-grav.jpg' : $users[ $like->user_id ]->file,
            ];
        }

        return $postsRepo;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param $auth
     * @param $postId
     * @return array
     */
    public function store($auth, $postId)
    {
        return $this->likes->getPostsOnUserAndPost($auth, $postId);
    }

    /**
     * Delete the like from the table
     *
     * @param  int $id
     * @return integer
     */
    public function delete($id)
    {
        return $this->likes->deleteLike($id);
    }

    /**
     * Store the like to a table
     *
     * @param $data
     * @return array
     */
    public function saveALike($data)
    {
        return $this->likes->saveLikes($data);
    }

    /**
     * Get total count of likes for the post
     *
     * @param $postId
     * @return mixed
     */
    public function getLikeCount($postId)
    {
        return $this->likes->getLikeCount($postId);
    }

    /**
     * Get a user who likes the post
     *
     * @param $postId
     * @return mixed
     */
    public function getPostsUser($postId)
    {
        return $this->likes->getPostsUser($postId);
    }

    /**
     * Get list of user to be shown in a modal
     *
     * @param $postId
     * @return mixed
     */
    public function getPostUsersForModal($postId)
    {
        return $this->likes->getPostsUser($postId);
    }
}