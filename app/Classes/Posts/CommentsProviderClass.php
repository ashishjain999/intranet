<?php namespace App\Classes\Posts;

use App\Repositories\Contract\CommentsInterface;

/**
 * Class CommentsProviderClass
 *
 * @package App\Classes\Posts
 */
class CommentsProviderClass
{

    /**
     * @var CommentsInterface
     */
    private $comments;

    /**
     * CommentsProviderClass constructor.
     *
     * @param CommentsInterface $comments
     */
    public function __construct(CommentsInterface $comments)
    {
        $this->comments = $comments;
    }

    /**
     * Get list of commentId & comments for a single post
     *
     * @param array $postId
     * @return array
     */
    public function getAllComments(Array $postId)
    {
        $commentId = [];
        //$commentsRepo = $this->comments->getAllComments([$postId]);
        $commentsRepo = $this->comments->getAllComments($postId);

        foreach ($commentsRepo as $comment) {
            $commentId[] = $comment->id;
        }

        return ['comments' => array_column($commentsRepo, null, 'id'), 'comment_id' => $commentId];
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param $data
     * @return array
     */
    public function store($data)
    {
        return $this->comments->saveComment($data);
    }

    /**
     * Permanently delete a comment
     *
     * @param $id
     * @return mixed
     */
    public function deleteAComment($id)
    {
        return $this->comments->deleteAComment($id);
    }

    /**
     * Get count of comments based on post
     *
     * @param $postId
     * @return mixed
     */
    public function countComments($postId)
    {
        return $this->comments->getCommentsCount($postId);
    }

    /**
     * Get list of commentId & comments for all posts (Unused for now)
     *
     * @param array $postId
     * @return array
     */
    public function getAllCommentsForAllPosts(Array $postId)
    {
        $commentId = [];
        $commentsRepo = $this->comments->getAllComments($postId);

        foreach ($commentsRepo as $comment) {
            $commentId[] = $comment->id;
        }

        return ['comments' => array_column($commentsRepo, null, 'id'), 'comment_id' => $commentId];
    }

    /**
     * Update a comment
     *
     * @param $data
     * @param $id
     * @return mixed
     */
    public function updateAComment($data, $id)
    {
        return $this->comments->updateAComment($data, $id);
    }
}