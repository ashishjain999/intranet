<?php namespace App\Classes\Posts;

use App\Repositories\Contract\PostsInterface;
use Auth;

/**
 * Class PostProviderClass
 *
 * @package App\Classes\Posts
 */
class PostsProviderClass
{

    /**
     * @var PostsInterface
     */
    private $posts;

    /**
     * PostProviderClass constructor.
     *
     * @param PostsInterface $posts
     */
    public function __construct(PostsInterface $posts)
    {
        $this->posts = $posts;
    }

    /**
     * Get list of all posts by pagination
     *
     * @param $users
     * @return array
     */
    public function getAllPostsByPagination($users)
    {
        $postsRepo = $this->posts->getAllPosts(config('config.pagination'));

        //To collect posts
        foreach ($postsRepo[ 'data' ] as $post) {
            $post->user_name = $users[ $post->user_id ]->name;
            $post->updated_at_date = strtoupper(date('d M y', strtotime($post->updated_at)));
            $post->updated_at_time = date('h:i A', strtotime($post->updated_at));

            if (isset($users[ $post->user_id ]->file)) {
                $post->photo = $users[ $post->user_id ]->file;
            } else {
                $post->photo = '/images/default-grav.jpg';
            }
        }

        $postId = [];
        foreach ($postsRepo[ 'data' ] as $post) {
            $postId[] = $post->id;
        }

        $posts = array_column($postsRepo[ 'data' ], null, 'id');
        $postsRepo[ 'data' ] = $posts;

        return ['post_repo' => $postsRepo, 'post_id' => $postId];

    }

    /**
     * Combine all the data to make all the posts
     *
     * @param $users
     * @param $postsRepo
     * @param $comments
     * @return mixed
     */
    public function getEntirePosts($users, $postsRepo, $comments)
    {
        if (isset($comments) & !empty($comments)) {
            foreach ($comments as $comment) {
                $commentsLikes = (isset($comments[ $comment->id ]->total_comments_likes)) ?
                    $commentsLikes = $comments[ $comment->id ]->total_comments_likes : [];


                $date = strtotime(date('Y-m-d H:i:s')) - strtotime($comment->created_at);
                $postsRepo[ 'post_repo' ][ 'data' ][ $comment->post_id ]->total_comments[] = [
                    'id'                   => $comment->id,
                    'user_id'              => $comment->user_id,
                    'user_name'            => $users[ $comment->user_id ]->name,
                    'post_id'              => $comment->post_id,
                    'text'                 => $comment->text,
                    'created_at'           => round(abs($date) / 3600, 0) . ' hours ago',
                    'total_comments_likes' => $commentsLikes,
                    'comment_photo'        => ($users[ $comment->user_id ]->file == null) ? '/images/default-grav.jpg' : $users[ $comment->user_id ]->file,
                ];
            }
        }

        return $postsRepo;
    }

    /**
     * Get a post by SEF (single post) + other details from users (URL)
     *
     * @param $sef
     * @param $users
     * @return array
     */
    public function getAPostBySEF($sef, $users)
    {
        $postsRepo = $this->posts->getAPostBySEF($sef);

        $postsRepo->user_name = $users[ $postsRepo->user_id ]->name;
        $postsRepo->designation = $users[ $postsRepo->user_id ]->designation;;
        $postsRepo->updated_at_date = strtoupper(date('d M y', strtotime($postsRepo->updated_at)));
        $postsRepo->updated_at_time = date('h:i A', strtotime($postsRepo->updated_at));

        if (isset($users[ $postsRepo->user_id ]->file)) {
            $postsRepo->photo = $users[ $postsRepo->user_id ]->file;
        } else {
            $postsRepo->photo = url('/images/default-grav.jpg');
        }

        return $postsRepo;
    }

    /**
     * Combine all the data to make it a post
     *
     * @param $users
     * @param $postsRepo
     * @param $comments
     * @return mixed
     */
    public function getEntirePost($users, $postsRepo, $comments)
    {
        if (isset($comments) & !empty($comments)) {
            foreach ($comments as $comment) {
                $commentsLikes = (isset($comments[ $comment->id ]->total_comments_likes)) ?
                    $commentsLikes = $comments[ $comment->id ]->total_comments_likes : [];

                $date = strtotime(date('Y-m-d H:i:s')) - strtotime($comment->created_at);

                $postsRepo->total_comments[] = [
                    'id'                   => $comment->id,
                    'user_id'              => $comment->user_id,
                    'user_name'            => $users[ $comment->user_id ]->name,
                    'post_id'              => $comment->post_id,
                    'text'                 => $comment->text,
                    'created_at'           => round(abs($date) / 3600, 0) . ' hours ago',
                    'total_comments_likes' => $commentsLikes,
                    'comment_photo'        => ($users[ $comment->user_id ]->file == null) ? url('/images/default-grav.jpg') : $users[ $comment->user_id ]->file,
                ];
            }
        }

        return $postsRepo;
    }

    /**
     * Could all the posts
     *
     * @return mixed
     */
    public function getCountPostRepo()
    {
        return $this->posts->getCountPostRepo();
    }

    /**
     * Store a newly created post in storage.
     *
     * @param object $request
     * @return array
     */
    public function store($request)
    {
        $sefUrl = str_slug($request[ 'post_title' ], '-');
        $data = [
            'user_id'    => Auth::id(),
            'title'      => $request[ 'post_title' ],
            'sef_url'    => $sefUrl,
            'text'       => $request[ 'post_body' ],
            'published'  => ($request[ 'published' ] == 'true') ? 1 : 0,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
        ];

        return $this->posts->savePost($data);
    }

    /**
     * Get a post based on id
     *
     * @param $request
     * @param $id
     * @return array & boolean
     */
    public function edit($request, $id)
    {
        $output = [];
        if ($request->user()->hasRole('admin')) {
            $output[ 'allowed' ] = true;
        }

        $output[ 'data' ] = $this->posts->getAPost($id);

        return $output;
    }

    /**
     * Update a post
     *
     * @param $request
     * @param $id
     * @return mixed
     */
    public function update($request, $id)
    {
        $data = [
            'title'      => $request[ 'title' ],
            'text'       => $request[ 'post_body' ],
            'published'  => isset($request[ 'published' ]) ? 1 : 0,
            'updated_at' => date('Y-m-d H:i:s'),
        ];

        return $this->posts->updateAPost($data, $id);
    }

    /**
     * Get all unpublished posts belongs to a user
     *
     * @param $userId
     * @return mixed
     */
    public function getUnpublishedPostsForAUser($userId)
    {
        return $this->posts->getUnpublishedPostsForAUser($userId);
    }

    /**
     * Get all published posts belongs to a user
     *
     * @param $userId
     * @return mixed
     */
    public function getPublishedPostsForAUser($userId)
    {
        return $this->posts->getPublishedPostsForAUser($userId);
    }
}