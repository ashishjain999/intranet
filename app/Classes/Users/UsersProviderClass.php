<?php namespace App\Classes\Users;

use App\Repositories\Contract\UsersInterface;

/**
 * Class UserProviderClass
 *
 * @package App\Classes\Users
 */
class UsersProviderClass
{

    /**
     * @var UsersInterface
     */
    private $users;

    /**
     * UsersProviderClass constructor.
     *
     * @param UsersInterface $users
     */
    public function __construct(UsersInterface $users)
    {
        $this->users = $users;
    }

    /**
     * Get all the list of users
     *
     * @return array
     */
    public function getAllUsers()
    {
        $usersRepo = $this->users->getAllUsers();

        return array_column($usersRepo, null, 'id');
    }

    /**
     * Get a user based on userId
     *
     * @param $userId
     * @return mixed
     */
    public function getAUser($userId)
    {
        return $this->users->getAUser($userId);
    }

    /**
     * Upload a photo for a user
     *
     * @param $image
     * @param $userId
     * @return \Illuminate\Contracts\Routing\UrlGenerator|string
     */
    public function uploadAPhoto($image, $userId)
    {
        $imageName = time() . '_' . $image->getClientOriginalName();
        $upload_success = $image->move(public_path('/public_images/' . $userId), $imageName);

        return url('/public_images/' . $userId . '/' . $imageName);
    }

    /**
     * Update a user profile
     *
     * @param $data
     * @param $userId
     * @return mixed
     */
    public function updateAUser($data, $userId)
    {
        return $this->users->updateAUser($data, $userId);
    }

    /**
     * Update the password of a user.
     *
     * @param $password
     * @param $userId
     * @return mixed
     */
    public function updatePassword($password, $userId)
    {
        return $this->users->updatePassword($password, $userId);
    }

    /**
     * Get all users based on pagination
     *
     * @param $perPage
     * @return mixed
     */
    public function getAllUsersWithPagination($perPage)
    {
        return $this->users->getAllUsersWithPagination($perPage);
    }

    /**
     * It will deactivate the user status
     *
     * @param $id
     * @return mixed
     */
    public function deactivateAUser($id)
    {
        return $this->users->deactivateAUser($id);
    }

    /**
     * It will activate the user status
     *
     * @param $id
     * @return mixed
     */
    public function activateAUser($id)
    {
        return $this->users->activateAUser($id);
    }
}