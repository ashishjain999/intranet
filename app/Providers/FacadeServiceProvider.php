<?php namespace App\Providers;

use Illuminate\Support\ServiceProvider;

/**
 * Class FacadeServiceProvider
 *
 * @package App\Providers
 */
class FacadeServiceProvider extends ServiceProvider
{

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton('objectcreator', 'App\Object\ObjectCreator');
    }
}