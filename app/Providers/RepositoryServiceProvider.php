<?php namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('App\Repositories\Contract\UsersInterface', 'App\Repositories\Db\UsersRepository');
        $this->app->bind('App\Repositories\Contract\PostsInterface', 'App\Repositories\Db\PostsRepository');
        $this->app->bind('App\Repositories\Contract\LikesInterface', 'App\Repositories\Db\LikesRepository');
        $this->app->bind('App\Repositories\Contract\CommentsInterface', 'App\Repositories\Db\CommentsRepository');
        $this->app->bind('App\Repositories\Contract\CommentsLikesInterface',
            'App\Repositories\Db\CommentsLikesRepository');
        $this->app->bind('App\Repositories\Contract\RoleUserInterface',
            'App\Repositories\Db\RoleUserRepository');
        $this->app->bind('App\Repositories\Contract\RolesInterface',
            'App\Repositories\Db\RolesRepository');

    }
}