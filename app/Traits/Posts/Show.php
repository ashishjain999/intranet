<?php namespace App\Traits\Posts;

trait Show
{

    /**
     * Return list of users beginning with a key
     *
     * @param $users
     * @return array
     */
    private function returnUsersWithKey($users)
    {
        return array_column($users, null, 'id');
    }
}

