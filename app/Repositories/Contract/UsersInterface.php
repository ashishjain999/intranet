<?php namespace App\Repositories\Contract;

/**
 * Interface PostsInterface
 *
 * @package App\Repositories\ContractZ
 */
interface UsersInterface extends RepositoryInterface
{

    /**
     * Get all users list
     *
     * @return mixed
     */
    public function getAllUsers();

    /**
     * Get all information about particular user
     *
     * @param int $id
     * @return mixed
     */
    public function getAUser(int $id);

    /**
     * Update information of a user
     *
     * @param array $data
     * @param int   $id
     * @return mixed
     */
    public function updateAUser(Array $data, int $id);

    /**
     * Get all users based on pagination
     *
     * @param int $page
     * @return mixed
     */
    public function getAllUsersWithPagination(int $page);

    /**
     * It will deactivate the user status
     *
     * @param $id
     * @return mixed
     */
    public function deactivateAUser($id);

    /**
     * It will activate the user status
     *
     * @param $id
     * @return mixed
     */
    public function activateAUser($id);

    /**
     * Change a user password
     *
     * @param $password
     * @param $id
     * @return mixed
     */
    public function updatePassword($password, $id);
}