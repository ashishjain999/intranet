<?php namespace App\Repositories\Contract;

/**
 * Interface PostsInterface
 *
 * @package App\Repositories\ContractZ
 */
interface PostsInterface extends RepositoryInterface
{

    /**
     * Get number of posts required
     *
     * @param int $page
     * @return mixed
     */
    public function getAllPosts(int $page);

    /**
     * Save post to a database
     *
     * @param array $data
     * @return array
     */
    public function savePost(Array $data);

    /**
     * Get total count of posts
     *
     * @return mixed
     */
    public function getCountPostRepo();

    /**
     * Get a post based on postId
     *
     * @param $postId
     * @return mixed
     */
    public function getAPost($postId);

    /**
     * Get a post based on post title (SEF URI)
     *
     * @param $sef
     * @return mixed
     */
    public function getAPostBySEF($sef);

    /**
     * Get all unpublished posts belongs to a user
     *
     * @param $userId
     * @return mixed
     */
    public function getUnpublishedPostsForAUser($userId);

    /**
     * Get all published posts belongs to a user
     *
     * @param $userId
     * @return mixed
     */
    public function getPublishedPostsForAUser($userId);

    /**
     * Update a post
     *
     * @param $data
     * @param $postId
     * @return mixed
     */
    public function updateAPost($data, $postId);

    //public function getACountOfPostBasedOnUser();

}