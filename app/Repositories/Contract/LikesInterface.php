<?php namespace App\Repositories\Contract;

/**
 * Interface PostsInterface
 *
 * @package App\Repositories\Contract
 */
interface LikesInterface extends RepositoryInterface
{

    /**
     * @param array $postId
     * @return mixed
     */
    public function getAllLikes(Array $postId);

    /**
     * Save comment to a database
     *
     * @param array $data
     * @return array
     */
    public function saveLikes(Array $data);

    /**
     * Get post by userId & postId
     *
     * @param $userId
     * @param $postId
     * @return mixed
     */

    public function getPostsOnUserAndPost($userId, $postId);

    /**
     * Delete a like
     *
     * @param $id
     * @return mixed
     */
    public function deleteLike($id);

    /**
     * Get count of likes for particular post
     *
     * @param $postId
     * @return mixed
     */
    public function getLikeCount($postId);

    /**
     * Get all users who liked the post
     *
     * @param $postId
     * @return mixed
     */
    public function getPostsUser($postId);
}