<?php namespace App\Repositories\Db;

use App\Repositories\Contract\RepositoryInterface;
use App;
use Db;

/**
 * Class Repository
 *
 * @package App\Repositories\Db
 */
abstract class Repository implements RepositoryInterface
{

}