<?php namespace App\Repositories\Db;

use App\Repositories\Contract\UsersInterface;
use DB;

Class UsersRepository extends Repository implements UsersInterface
{

    private $table = 'users';

    /**
     * Display all posts
     *
     * @return mixed
     */
    public function getAllUsers()
    {
        return DB::table($this->table)
                 ->select('id', 'name', 'email', 'file', 'designation')
                 ->get()
                 ->toArray();
    }

    /**
     * Get all information about particular user
     *
     * @param int $id
     * @return mixed
     */
    public function getAUser(int $id)
    {
        return DB::table('users')
                 ->select('id', 'name', 'email', 'team', 'extn_no', 'designation',
                     'mobile', 'location', 'birthday', 'about_me', 'file', 'user_status')->where('id', $id)->first();
    }

    /**
     * Update information of a user
     *
     * @param array $data
     * @param int   $id
     * @return mixed
     */
    public function updateAUser(Array $data, int $id)
    {
        return DB::table('users')->where('id', $id)->update($data);
    }

    /**
     * Get all users based on pagination
     *
     * @param int $page
     * @return mixed
     */
    public function getAllUsersWithPagination(int $page)
    {
        return DB::table($this->table)
                 ->select('id', 'name', 'email', 'user_status')
                 ->paginate($page);
    }

    /**
     * It will deactivate the user status
     *
     * @param $id
     * @return mixed
     */
    public function deactivateAUser($id)
    {
        return DB::table($this->table)->where('id', $id)->update(['user_status' => 0]);
    }

    /**
     * It will activate the user status
     *
     * @param $id
     * @return mixed
     */
    public function activateAUser($id)
    {
        return DB::table($this->table)->where('id', $id)->update(['user_status' => 1]);
    }

    /**
     * Change a user password
     *
     * @param $password
     * @param $id
     * @return mixed
     */
    public function updatePassword($password, $id)
    {
        return DB::table($this->table)->where('id', $id)->update(['password' => $password]);
    }
}