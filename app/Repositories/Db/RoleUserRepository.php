<?php namespace App\Repositories\Db;

use App\Repositories\Contract\RoleUserInterface;
use DB;
use Auth;

Class RoleUserRepository extends Repository implements RoleUserInterface
{

    private $table = 'role_user';

    /**
     * Update information of a user
     *
     * @param array $data
     * @return mixed
     */
    public function saveRoleUser(Array $data)
    {
        return DB::table($this->table)->insert([
            'role_id'    => $data[ 'role_id' ],
            'user_id'    => $data[ 'user_id' ],
            'created_at' => $data[ 'created_at' ],
            'updated_at' => $data[ 'updated_at' ]
        ]);
    }

    /**
     * Update user's role
     *
     * @param $data
     * @param $userId
     * @return mixed
     */
    public function updateRoleOfUser($data, $userId)
    {
        return DB::table($this->table)->where('user_id', $userId)->update($data);
    }

    /**
     * Get role of a user based on user_id
     *
     * @param $id
     * @return mixed
     */
    public function getRoleUser($id)
    {
        return DB::table($this->table)->select('role_id', 'user_id')->where('user_id', $id)->first();
    }
}