<?php namespace App\Repositories\Db;

use App\Repositories\Contract\CommentsInterface;
use DB;

Class CommentsRepository extends Repository implements CommentsInterface
{

    private $table = 'comments';

    /**
     * Display comments belongs to a post
     *
     * @param array $postId
     * @return mixed
     */
    public function getAllComments(Array $postId)
    {
        return DB::table($this->table)
                 ->select('id', 'user_id', 'post_id', 'text', 'created_at')
                 ->whereIn('post_id', $postId)
                 ->get()
                 ->toArray();
    }

    /**
     * Save comment to a database
     *
     * @param array $data
     * @return array
     */
    public function saveComment(Array $data)
    {
        return DB::table($this->table)->insertGetId([
            'user_id'    => $data[ 'user_id' ],
            'post_id'    => $data[ 'post_id' ],
            'text'       => $data[ 'text' ],
            'created_at' => $data[ 'created_at' ],
            'updated_at' => $data[ 'updated_at' ],
        ]);
    }

    /**
     * Get count of comments
     *
     * @param $postId
     * @return mixed
     */
    public function getCommentsCount($postId)
    {
        return DB::table($this->table)->where('post_id', $postId)->count();
    }

    /**
     * Permanent delete of a comment
     *
     * @param $id
     * @return mixed
     */
    public function deleteAComment($id)
    {
        return DB::table($this->table)->where('id', $id)->delete();
    }

    /**
     * Update a comment
     *
     * @param $data
     * @param $id
     * @return mixed
     */
    public function updateAComment($data, $id)
    {
        return DB::table($this->table)->where('id', $id)->update($data);
    }
}