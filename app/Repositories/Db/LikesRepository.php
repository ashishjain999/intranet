<?php namespace App\Repositories\Db;

use App\Repositories\Contract\CommentsInterface;
use App\Repositories\Contract\LikesInterface;
use App\Repositories\Contract\PostsInterface;
use DB;

Class LikesRepository extends Repository implements LikesInterface
{

    private $table = 'likes';

    /**
     * Display likes belongs to a post
     *
     * @param array $postId
     * @return mixed
     */
    public function getAllLikes(Array $postId)
    {
        return DB::table($this->table)
                 ->select('id', 'user_id', 'post_id')
                 ->whereIn('post_id', $postId)
                 ->get()
                 ->toArray();
    }

    /**
     * Save likes to a database
     *
     * @param array $data
     * @return array
     */
    public function saveLikes(Array $data)
    {
        return DB::table($this->table)->insert([
            'user_id'    => $data[ 'user_id' ],
            'post_id'    => $data[ 'post_id' ],
            'created_at' => $data[ 'created_at' ],
            'updated_at' => $data[ 'updated_at' ],
        ]);
    }

    /**
     * Get post by userId & postId
     *
     * @param $userId
     * @param $postId
     * @return mixed
     */
    public function getPostsOnUserAndPost($userId, $postId)
    {
        return DB::table($this->table)->where([
            'user_id' => $userId,
            'post_id' => $postId
        ])->select('id')->first();
    }

    /**
     * Delete a like
     *
     * @param $id
     * @return mixed
     */
    public function deleteLike($id)
    {
        return DB::table($this->table)->where('id', '=', $id)->delete();
    }

    /**
     * Get count of likes for particular post
     *
     * @param $postId
     * @return mixed
     */
    public function getLikeCount($postId)
    {
        return DB::table($this->table)->where('post_id', $postId)->count();
    }

    /**
     * Get all users who liked the post
     *
     * @param $postId
     * @return mixed
     */
    public function getPostsUser($postId)
    {
        return DB::table($this->table)->join('users', 'users.id', '=',
            $this->table . '.user_id')->select('users.id', 'name', 'file')->where('post_id',
            $postId)->distinct()->get()->toArray();
    }
}