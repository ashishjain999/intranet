<?php namespace App\Repositories\Db;

use App\Repositories\Contract\PostsInterface;
use DB;

Class PostsRepository extends Repository implements PostsInterface
{

    private $table = 'posts';

    /**
     * Display all posts
     *
     * @param int $page
     * @return mixed
     */
    public function getAllPosts(int $page)
    {
        return DB::table($this->table)
                 ->select('id', 'user_id', 'title', 'sef_url', 'text', 'updated_at')
                 ->where('published', 1)
                 ->orderBy('id', 'desc')
                 ->simplePaginate($page)
                 ->toArray();
    }

    /**
     * Save post to a database
     *
     * @param array $data
     * @return array
     */
    public function savePost(Array $data)
    {
        return DB::table($this->table)->insert([
            'user_id'    => $data[ 'user_id' ],
            'title'      => $data[ 'title' ],
            'sef_url'    => $data[ 'sef_url' ],
            'text'       => $data[ 'text' ],
            'published'  => $data[ 'published' ],
            'created_at' => $data[ 'created_at' ],
            'updated_at' => $data[ 'updated_at' ],
        ]);
    }

    /**
     * Get total count of posts
     *
     * @return mixed
     */
    public function getCountPostRepo()
    {
        return DB::table($this->table)->count();
    }

    /**
     * Get a post based on postId
     *
     * @param $postId
     * @return mixed
     */
    public function getAPost($postId)
    {
        return DB::table($this->table)
                 ->select('id', 'user_id', 'title', 'text', 'published', 'updated_at')
                 ->where('id', $postId)->first();
    }

    /**
     * Get a post based on post title (SEF URI)
     *
     * @param $sef
     * @return mixed
     */
    public function getAPostBySEF($sef)
    {
        return DB::table($this->table)
                 ->select('id', 'user_id', 'title', 'sef_url', 'text', 'updated_at')
                 ->where('sef_url', $sef)->first();
    }

    /**
     * Get all unpublished posts belongs to a user
     *
     * @param $userId
     * @return mixed
     */
    public function getUnpublishedPostsForAUser($userId)
    {
        return DB::table($this->table)->select('id', 'title', 'sef_url', 'text')
                 ->where('user_id', $userId)
                 ->where('published', 0)->get();
    }

    /**
     * Get all published posts belongs to a user
     *
     * @param $userId
     * @return mixed
     */
    public function getPublishedPostsForAUser($userId)
    {
        return DB::table($this->table)->select('id', 'title', 'sef_url', 'text')
                 ->where('user_id', $userId)
                 ->where('published', 1)->get();
    }

    /**
     * Update a post
     *
     * @param $data
     * @param $postId
     * @return mixed
     */
    public function updateAPost($data, $postId)
    {
        return DB::table('posts')->where('id', $postId)->update($data);
    }
}