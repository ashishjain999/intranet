<?php namespace App\Repositories\Db;

use App\Repositories\Contract\CommentsLikesInterface;
use DB;

Class CommentsLikesRepository extends Repository implements CommentsLikesInterface
{

    private $table = 'comments_likes';

    /**
     * Display comments belongs to a post
     *
     * @param array $commentId
     * @return mixed
     */
    public function getAllCommentsLikes(Array $commentId)
    {
        return DB::table($this->table)
                 ->select('id', 'comment_id', 'user_id')
                 ->whereIn('comment_id', $commentId)
                 ->get()
                 ->toArray();
    }

    /**
     * Save comment like to a database
     *
     * @param array $data
     * @return array
     */
    public function saveCommentLike(Array $data)
    {
        return DB::table($this->table)->insert([
            'user_id'    => $data[ 'user_id' ],
            'comment_id' => $data[ 'comment_id' ],
            'created_at' => $data[ 'created_at' ],
            'updated_at' => $data[ 'updated_at' ],
        ]);
    }

    /**
     * Get comment by userId & postId
     *
     * @param $userId
     * @param $commentId
     * @return mixed
     */
    public function getCommentsOnUserAndPost($userId, $commentId)
    {
        return DB::table($this->table)->where([
            'user_id'    => $userId,
            'comment_id' => $commentId
        ])->select('id')->first();
    }

    /**
     * Delete a comment like
     *
     * @param $id
     * @return mixed
     */
    public function deleteLike($id)
    {
        return DB::table($this->table)->where('id', '=', $id)->delete();
    }

    /**
     * Get count of likes for particular comment
     *
     * @param $commentId
     * @return mixed
     */
    public function getLikeCount($commentId)
    {
        return DB::table($this->table)->where('comment_id', $commentId)->count();
    }

    /**
     * Get all users who liked the comments
     *
     * @param $commentId
     * @return mixed
     */
    public function getCommentsUsers($commentId)
    {
        return DB::table($this->table)->join('users', 'users.id', '=',
            $this->table . '.user_id')->select('users.id', 'name', 'file')->where('comment_id',
            $commentId)->distinct()->get()->toArray();
    }
}