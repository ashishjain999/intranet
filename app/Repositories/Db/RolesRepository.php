<?php namespace App\Repositories\Db;

use App\Repositories\Contract\RolesInterface;
use DB;

Class RolesRepository extends Repository implements RolesInterface
{

    private $table = 'roles';

    /**
     * Get all the roles defined in a system
     *
     * @return mixed
     */
    public function getAllRoles()
    {
        return DB::table($this->table)->select('id', 'name')->get();
    }
}