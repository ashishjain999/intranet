<?php namespace App\Http\Facades;

use Illuminate\Support\Facades\Facade;

/**
 * Class ObjectCreator
 *
 * @package App\Http\Facades\Repository
 */
class ObjectCreatorFacade extends Facade
{
    /**
     * @return string
     */
    public static function getFacadeAccessor()
    {
        return 'objectcreator';
    }
}