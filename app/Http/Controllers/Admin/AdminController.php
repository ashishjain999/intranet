<?php namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;

/**
 * Class AdminController
 *
 * @package App\Http\Controllers\Admin
 */
class AdminController extends Controller
{

    /**
     * AdminController constructor.
     */
    public function __construct()
    {
        $this->middleware('role:admin');
    }

    /**
     * Shows the admin dashboard
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        return view('admin.index');
    }


}
