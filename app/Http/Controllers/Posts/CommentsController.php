<?php namespace App\Http\Controllers\Posts;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Facades\ObjectCreatorFacade;
use Auth;

/**
 * Class CommentsController
 *
 * @package App\Http\Controllers\Posts
 */
class CommentsController extends Controller
{

    /**
     * @var object
     */
    private $commentConnect;
    private $userConnect;

    /**
     * CommentsController constructor.
     * It will create a common variable for ObjectCreatorFacade
     */
    public function __construct()
    {
        $this->commentConnect = ObjectCreatorFacade::object('App\Classes\Posts\CommentsProviderClass');
        $this->userConnect = ObjectCreatorFacade::object('App\Classes\Users\UsersProviderClass');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return void
     */
    public function store(Request $request)
    {
        $data = [
            'user_id'    => Auth::id(),
            'post_id'    => $request[ 'post_id' ],
            'text'       => $request[ 'comment' ],
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
        ];

        $insert = $this->commentConnect->store($data);
        //$insert = $this->commentsRepo->saveComment($data);

        $data[ 'id' ] = $insert;
        $data[ 'user_name' ] = Auth::user()->name;
        $data[ 'count' ] = $this->commentConnect->countComments($request[ 'post_id' ]);

        $date = strtotime(date('Y-m-d H:i:s')) - strtotime($data[ 'created_at' ]);
        $data[ 'created_at' ] = round(abs($date) / 3600, 0) . ' hours ago';

        //Get a user photo
        $commentPhoto = $this->userConnect->getAUser(Auth::id());
        $data[ 'comment_photo' ] = ($commentPhoto->file == null) ? '/images/default-grav.jpg' : $commentPhoto->file;

        return response()->json($data, 200);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Request $request
     * @param  int    $id
     * @return array
     */
    public function destroy(Request $request, $id)
    {
        $this->commentConnect->deleteAComment($id);

        return [
            'post_id' => $request[ 'post_id' ],
            'count'   => $this->commentConnect->countComments($request[ 'post_id' ])
        ];
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int                      $id
     * @return int
     */
    public function update(Request $request, $id)
    {
        $data = [
            'text'       => $request[ 'comment' ],
            'updated_at' => date('Y-m-d H:i:s'),
        ];

        return $this->commentConnect->updateAComment($data, $id);
    }
}
