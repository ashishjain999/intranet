<?php namespace App\Http\Controllers\Posts;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Facades\ObjectCreatorFacade;
use Auth;

/**
 * Class PostLikesController
 *
 * @package App\Http\Controllers\Posts
 */
class PostLikesController extends Controller
{

    /**
     * @var object
     */
    private $postLikeConnect;

    /**
     * CommentsController constructor.
     * It will create a common variable for ObjectCreatorFacade
     */
    public function __construct()
    {
        $this->postLikeConnect = ObjectCreatorFacade::object('App\Classes\Posts\PostsLikesProviderClass');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function store(Request $request)
    {

        //$data = ObjectCreatorFacade::object('App\Classes\Posts\PostsLikesProviderClass');

        $query = $this->postLikeConnect->store(Auth::id(), $request[ 'post_id' ]);

        if ( !empty($query)) {

            //Delete entire record from Likes table
            $this->postLikeConnect->delete($query->id);

            $count = $this->postLikeConnect->getLikeCount($request[ 'post_id' ]);
            $users = $this->postLikeConnect->getPostsUser($request[ 'post_id' ]);
            $single = 0;
            if (count($users) == 1) {
                $single = 1;
            }

            return [
                'post_id' => $request[ 'post_id' ],
                'count'   => $count,
                'msg'     => 'deleted',
                'users'   => $users,
                'single'  => $single
            ];

        } else {

            $store = [
                'user_id'    => Auth::id(),
                'post_id'    => $request[ 'post_id' ],
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ];

            //Saving likes
            $this->postLikeConnect->saveALike($store);

            $count = $this->postLikeConnect->getLikeCount($request[ 'post_id' ]);
            $users = $this->postLikeConnect->getPostsUser($request[ 'post_id' ]);
            $single = 0;
            if (count($users) == 1) {
                $single = 1;
            }

            return [
                'post_id' => $request[ 'post_id' ],
                'count'   => $count,
                'msg'     => 'success',
                'users'   => $users,
                'single'  => $single
            ];

        }

    }

    /**
     * Returns the name, image of a person who liked the post to be shown in a modal.
     *
     * @param Request $request
     * @return array
     */
    public function getPostUsers(Request $request)
    {
        return $this->postLikeConnect->getPostUsersForModal($request[ 'post_id' ]);
    }
}
