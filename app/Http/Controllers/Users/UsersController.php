<?php namespace App\Http\Controllers\Users;

use App\Http\Requests\UpdateUserPassword;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Facades\ObjectCreatorFacade;
use Auth;

/**
 * Class UsersController
 *
 * @package App\Http\Controllers\Users
 */
class UsersController extends Controller
{

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //Check whether a user is correct
        if (Auth::id() != $id) {
            return abort(404);
        }

        $user = ObjectCreatorFacade::object('App\Classes\Users\UsersProviderClass')
                                   ->getAUser($id);

        $posts = ObjectCreatorFacade::object('App\Classes\Posts\PostsProviderClass');
        $unpublishedPosts = $posts->getUnpublishedPostsForAUser(Auth::id());
        $publishedPosts = $posts->getPublishedPostsForAUser(Auth::id());

        return view('users.profile',
            ['user' => $user, 'unpublished' => $unpublishedPosts, 'published' => $publishedPosts]);


    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int                      $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (Auth::id() != $id) {
            return abort(404);
        }

        $data = [
            'about_me'    => $request[ 'about_me' ],
            'team'        => $request[ 'team' ],
            'extn_no'     => $request[ 'extno' ],
            'designation' => $request[ 'designation' ],
            'mobile'      => $request[ 'mobile' ],
            'location'    => $request[ 'location' ],
            'birthday'    => date('Y-m-d 00:00:00', strtotime($request[ 'birthday' ])),
            'updated_at'  => date('Y-m-d H:i:s'),
        ];

        $objUser = ObjectCreatorFacade::object('App\Classes\Users\UsersProviderClass');

        if ($request->file('image_file')) {
            $data[ 'file' ] = $objUser->uploadAPhoto($request->file('image_file'), $id);
        }


        $objUser->updateAUser($data, $id);

        $request->session()->flash('alert', 'Profile was successfully updated');

        return response()->redirectTo('/users/' . $id, 302);
    }

    /**
     * Show the change password page
     *
     * @return \Illuminate\Http\Response
     */
    public function changePassword()
    {
        return view('auth.passwords.change-password');
    }

    /**
     * Update the password in a user.
     *
     * @param UpdateUserPassword $request
     * @return \Illuminate\Http\Response
     */
    public function updatePassword(UpdateUserPassword $request)
    {
        ObjectCreatorFacade::object('App\Classes\Users\UsersProviderClass')
                           ->updatePassword(bcrypt($request[ 'password' ]), Auth::id());

        $request->session()->flash('alert', 'Password was successfully updated');

        return response()->redirectTo('/settings/change-password/', 302);
    }
}
