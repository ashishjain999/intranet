@extends('layouts.app')
@push('scripts')
    <!--User script-->
    <script src="{{ asset('js/my/user.js') }}"></script>
@endpush
@section('content')
    <div class="page-header">
        <div class="container">
            @if(Session::has('alert'))
                <div class="alert alert-dismissible alert-success">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                    <strong>{{ Session::get('alert') }}</strong>
                </div>
            @endif<!-- end flash-message -->
            <form class="form-horizontal" role="form" method="POST" action="{{ url('/settings/change-password') }}">
                {{ csrf_field() }}
                {{ method_field('PUT') }}
                <div class="row">
                    <div class="col-sm-12">
                        <h2>Change Password</h2>
                    </div>
                    <div class="col-sm-12">
                        @if ($errors->has('password'))
                            <span class="help-block text-danger">
                                <strong>{{ $errors->first('password') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="col-sm-12">
                        <div class="form-group">
                            <label for="newPassword">New Password</label>
                            <input type="password" class="form-control" id="newPassword" placeholder="Password"
                                   name="password">
                        </div>
                    </div>
                    <div class="col-sm-12">
                        <div class="form-group">
                            <label for="confirmPassword">Confirm Password</label>
                            <input type="password" class="form-control" id="confirmPassword" placeholder="Password"
                                   name="password_confirmation">
                        </div>
                    </div>
                    <div class="col-sm-12">
                        <button type="submit" class="btn btn-primary btn-sm"><i class="fa fa-sign-in"></i>
                            Update password
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection