@extends('layouts.app')
@push('scripts')
    <!--Home page scripts-->
    <script src="//cdn.ckeditor.com/4.9.2/standard/ckeditor.js"></script>
    <script src="{{ asset('js/ckeditor.js') }}"></script>
    <script src="{{ asset('js/my/custom.js') }}"></script>
@endpush
@section('content')

    <div class="row">
        <div class="col-sm-12">
            @if (session('alert'))
                <div class="alert alert-success">
                    {{ session('alert') }}
                </div>
            @endif
            <form role="form" enctype="multipart/form-data" method="post" id="form_post"
                  action="{{url('/posts/'.$post->id)}}">
                {{ csrf_field() }}
                {{ method_field('PUT') }}
                <div class="form-group">
                    <label for="title">Title</label>
                    <input type="text" class="form-control" name="title" placeholder="Title"
                           value="{{$post->title}}" required>
                </div>
                <div class="form-group">
                    <label for="post_body">What's on your mind?</label>
                    <textarea class="form-control form-post-textarea" rows="3" name="post_body"
                              required>{{$post->text}}</textarea>
                </div>
                <div class="form-check">
                    <label class="form-check-label">
                        <input class="form-check-input" type="checkbox" value="true"
                               name="published" {{$post->published==1?'checked':''}}>
                        Publish this article
                    </label>
                </div>
                <div class="form-group">
                    <div class="pull-right">
                        <input type="submit" class="btn btn-primary btn-sm" value="Update Changes">
                    </div>
                </div>
            </form>
        </div>
    </div>

@endsection