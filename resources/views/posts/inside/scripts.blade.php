@push('scripts')
    <!--Home page scripts-->
    <script src="//cdn.ckeditor.com/4.9.2/standard/ckeditor.js"></script>
    <script src="{{ asset('js/ckeditor.js') }}"></script>
    <script src="{{ asset('js/my/pnotify.js') }}"></script>
    <script src="{{ asset('js/my/custom.js') }}"></script>
    {{--<script src="https://cdn.ckeditor.com/ckeditor5/10.0.1/classic/ckeditor.js"></script>--}}
@endpush