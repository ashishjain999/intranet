<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <META NAME="ROBOTS" CONTENT="NOINDEX, NOFOLLOW">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }} - @yield('title')</title>

    <!-- Styles -->
    <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="//fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
    <link href="//stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
    <link rel="stylesheet" href="//ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/smoothness/jquery-ui.css">
    <link href="{{asset('css/pnotify.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('css/pnotify.brighttheme.css') }}" rel="stylesheet">
    <link href="{{ asset('css/my/custom.css') }}" rel="stylesheet">

    <!--Common scripts-->
    <!--//--><![CDATA[//><!--
    var auth = {{Auth::id()}}
        //--><!]]>

</head>
<body>

<nav class="navbar navbar-expand-lg fixed-top navbar-dark bg-primary">
    <div class="container">
        <!-- Branding Image -->
        <a class="navbar-brand" href="{{ url('/posts') }}">
            <img src="{{ config('app.logo', 'Intranet') }}" alt="Cactus Intranet logo">
        </a>

        <!-- Collapsed Hamburger -->
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarColor01"
                aria-controls="navbarColor01" aria-expanded="false" aria-label="Toggle navigation" style="">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarColor01">
            <!-- Left Side Of Navbar -->
            @guest

            @else
                <ul class="navbar-nav">
                    <li class="nav-item">
                        <a class="nav-link" href="/posts">HOME</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="javscript:;">ARTICLES</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="javscript:;">GROUPS</a>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" id="download"
                           aria-expanded="false">MORE<span class="caret"></span></a>
                        <div class="dropdown-menu" aria-labelledby="download">
                            <a class="dropdown-item" href=javscript:;>ARIGATOS</a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" href="http://sp.cactusglobal.com/" target="_blank">SHAREPOINT</a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" href="https://sp.cactusglobal.com/ims/Pages/default.aspx"
                               target="_blank">IMS</a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" href=javscript:;>HR-BOARD</a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" href=javscript:;>IT SERVICE REQUEST</a>
                            <a class="dropdown-item" href="https://wiki.editage.com/" target="_blank">EDITAGE WIKI</a>
                            <a class="dropdown-item" href="http://www.editage.com/insights/" target="_blank">EDITAGE
                                INSIGHTS</a>
                            <a class="dropdown-item" href="https://outlook.office.com/owa/" target="_blank">CACTUS
                                WEBMAIL</a>
                            <a class="dropdown-item" href="im:sip:itsupport@onecactus.com">CHAT WITH IT
                                SUPPORT<sup>Beta</sup> </a>

                        </div>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="http://timetrack.cactusglobal.com/ess" target="_blank">TIMETRACK</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="http://itsupport.cactusglobal.com" target="_blank">IT SUPPORT</a>
                    </li>
                </ul>
        @endguest


        <!-- Right Side Of Navbar -->
            <ul class="nav navbar-nav ml-auto ">
                @guest
                    <li class="nav-item"><a class="nav-link" href="{{ route('login') }}">Login</a></li>
                    {{--<li class="nav-item"><a class="nav-link" href="{{ route('register') }}">Register</a></li>--}}
                @else
                    <li class="nav-item dropdown">
                        <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown" role="button"
                           aria-expanded="false" aria-haspopup="true" v-pre>
                            {{ Auth::user()->name }} <span class="caret"></span>
                        </a>
                        <div class="dropdown-menu" aria-labelledby="themes">
                            @if(request()->user()->hasRole('admin'))
                                <a class="dropdown-item" href='{{url('admin')}}'>Admin</a>
                            @endif
                            <a class="dropdown-item" href='{{url('users/'.Auth::id())}}'>Profile</a>
                            <a class="dropdown-item" href='{{url('settings/change-password')}}'>Change Password</a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" href="{{ route('logout') }}"
                               onclick="event.preventDefault();
                                                 document.getElementById('logout-form').submit();">
                                Logout
                            </a>
                            <form id="logout-form" action="{{ route('logout') }}" method="POST"
                                  style="display: none;">
                                {{ csrf_field() }}
                            </form>
                        </div>
                    </li>
                @endguest
            </ul>
        </div>
    </div>
</nav>

<div class="page-header">
    <div class="container">
        @yield('content')
    </div>
</div>

<!-- Scripts -->
@include('common.js')

</body>
</html>
