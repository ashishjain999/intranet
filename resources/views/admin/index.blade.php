@extends('layouts.app')
@push('scripts')
    <!--User script-->
    {{--<script src="{{ asset('js/my/user.js') }}"></script>--}}
@endpush
@section('content')
    <div class="row my-2">
        <div class="col-lg-12">
            <h3>Admin Activity</h3>
            <h4><u>Users</u></h4>
            <h6><a href="{{url('admin/users/view-users')}}" class="btn btn-sm">View all users</a></h6>
            <h6><a href="{{url('admin/users/add-new-user')}}" class="btn btn-sm">Add a new user</a></h6>
            <h4><u>Other Activity</u></h4>
            <h6>...</h6>
        </div>
    </div>

@endsection