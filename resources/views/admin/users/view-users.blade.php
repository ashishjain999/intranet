@extends('layouts.app')
@push('scripts')
    <!--User script-->
    {{--<script src="{{ asset('js/my/user.js') }}"></script>--}}
@endpush
@section('content')
    <div class="row my-2">
        <div class="col-lg-12">
            <h4><u>Users</u><a href="{{url('admin/users/add-new-user')}}" class="btn btn-sm">Add a new user</a></h4>

            @if(Session::has('alert'))
                <div class="alert alert-dismissible alert-success">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                    <strong>{{ Session::get('alert') }}</strong>
                </div>
            @endif <!-- end flash-message -->
            <table class="table table-hover">
                <tr>
                    <td>Id</td>
                    <td>Email</td>
                    <td>Name</td>
                    <td>Option</td>
                </tr>
                @foreach($users as $user)
                    <tr>
                        <td>{{$user->id}}</td>
                        <td>
                            @if($user->id == 1)
                                {{$user->email}}
                            @else
                                <a href="{{url('/admin/users/'.$user->id.'/edit-user')}}">{{$user->email}}</a>
                            @endif
                        </td>
                        <td>{{$user->name}}</td>
                        <td>
                            @if($user->id == 1)
                                Can't take any action.
                            @else
                                <a href="{{url('/admin/users/'.$user->id.'/edit-user')}}">Edit</a> |
                                <a href="{{url('/admin/users/'.$user->id)}}{{($user->user_status == 1) ? '/deactivate-user' : '/activate-user'}}">
                                    {{($user->user_status == 1) ? 'Deactivate' : 'Activate'}}
                                </a>
                            @endif
                        </td>
                    </tr>
                @endforeach
            </table>
            {{ $users->links()}}
        </div>

    </div>
@endsection