@extends('layouts.app')
@push('scripts')
    <!--User script-->
    <script src="{{ asset('js/my/user.js') }}"></script>
@endpush
@section('content')
    @if(Session::has('alert'))
        <div class="alert alert-dismissible alert-success">
            <button type="button" class="close" data-dismiss="alert">×</button>
            <strong>{{ Session::get('alert') }}</strong>
        </div>
    @endif<!-- end flash-message -->
    <form role="form" method="post" id="form_post" action="{{'/admin/users/'.$user->id.'/update-user'}}">
        {{ csrf_field() }}
        {{ method_field('PUT') }}
        <div class="row">
            <div class="col-lg-12">
                <h3>Edit {{$user->name."'s"}} user</h3>
                <hr>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-7">
                <h6><u>ABOUT ME</u></h6>
                <div class="form-group">
                    <input name="about_me" type="text" class="form-control" id="about_me"
                           placeholder="Please add it."
                           value="{{$user->about_me}}">
                </div>
                <h6><u>CONTACT INFORMATION</u></h6>
                <div class="form-group">
                    <label>Email address</label>
                    <input name="email" type="email" class="form-control" id="extno"
                           value="{{$user->email}}">
                    <small id="emailHelp" class="form-text text-muted">We'll never share your email with
                        anyone else.
                    </small>
                </div>
                <div class="form-group">
                    <label>Password</label>
                    <input name="password" type="password" class="form-control" id="password">
                    <small id="emailHelp" class="form-text text-muted">Change user's password
                    </small>
                </div>
                <div class="form-group">
                    <label>Name</label>
                    <input name="name" type="text" class="form-control" id="extno"
                           value="{{$user->name}}">
                </div>
                @if($user->id != 1)
                    <div class="form-group">
                        <label>Role</label>
                        <select name="role" class="form-control">
                            <option>Select</option>
                            @foreach($roles as $role)
                                <option value="{{$role->id}}" {{$role_user->role_id == $role->id ? 'selected':''}}>{{$role->name}}</option>
                            @endforeach
                        </select>
                        <small id="emailHelp" class="form-text text-muted">Default role is user.</small>
                    </div>
                @endif
                <div class="form-group">
                    <label for="team">Team</label>
                    <select name="team" class="form-control">
                        <option value="{{null}}">Select</option>
                        @foreach(config('config.team') as $key => $team)
                            <option value="{{$key}}" {{$key == $user->team ? 'selected' : ''}}>{{$team}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <label for="extno">Extn.No.</label>
                    <input name="extno" type="number" class="form-control" id="extno"
                           placeholder="Extension Number"
                           value="{{$user->extn_no}}">
                </div>
                <div class="form-group">
                    <label for="designation">Designation</label>
                    <input name="designation" type="text" class="form-control" id="designation"
                           placeholder="Designation"
                           value="{{$user->designation}}">
                </div>
                <div class="form-group">
                    <label for="mobile">Mobile</label>
                    <input name="mobile" type="number" class="form-control" id="mobile"
                           placeholder="Mobile"
                           value="{{$user->mobile}}">
                </div>
                <div class="form-group">
                    <label for="location">Location</label>
                    <input name="location" type="text" class="form-control" id="location"
                           placeholder="Location"
                           value="{{$user->location}}">
                </div>
                <div class="form-group">
                    <label for="birthday">Birthday</label>
                    <input name="birthday" type="text" class="form-control" id="birthday"
                           placeholder="Birthday"
                           value="{{$user->birthday}}">
                </div>
                <div class="form-group row">

                    <div class="col-lg-9">
                        <input type="submit" class="btn btn-primary btn-sm" value="Save Changes">
                    </div>
                </div>
            </div>
            <div class="col-lg-5">
                <div class="unpublished-posts">
                    <h6><u>My Unpublished Posts</u></h6>
                    <ul class="unpublished-posts-listing">
                        @foreach($unpublished as $post)
                            <li class="items">
                                <a href='{{url('/posts/'.$post->id.'/edit')}}'>{{$post->title}}</a><br/>
                                <?php $text = strip_tags($post->text) ?>
                                @if(strlen($text) > 50)
                                    <?php echo substr($text, 0, strpos($text, ' ', 50)) . '...' ?>
                                @else
                                    <?php echo $text; ?>
                                @endif
                            </li>
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>
    </form>

@endsection