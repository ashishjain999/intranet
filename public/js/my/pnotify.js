//Plugin for deleting a comment
$.fn.alertPnotify = function (options) {
    var settings = $.extend({
        title: 'Pnotify',
        text: 'Pnotify text',
        type: 'success',
        delay: 2000
    }, options);
    return new PNotify(settings);
}