<?php

return [

    /**
    |--------------------------------------------------------------------------
    | Team list
    |--------------------------------------------------------------------------
    |
    | Here is the list of Team.
    |
    */

    'team' => [
        '0'  => 'Web &amp; Graphics',
        '1'  => 'Computer Science and Mathematics',
        '2'  => 'Transcription',
        '3'  => 'HR',
        '4'  => 'Physical Sciences',
        '5'  => 'Technology',
        '6'  => 'Medical Communication',
        '7'  => 'Finance',
        '8'  => 'Human Resources',
        '9'  => 'Global Talent Acquisition',
        '10' => 'Marketing',
        '11' => 'PJS',
        '12' => 'Skill and Knowledge Management',
        '13' => 'Product Management',
        '14' => 'Customer Relations',
        '15' => 'Medical Communications',
        '16' => 'Medlife',
        '17' => 'Digital Marketing',
        '18' => 'Helpdesk',
        '19' => 'Medicine',
        '20' => 'CDD',
        '21' => 'IT Team',
        '22' => 'Humanities',
        '23' => 'Physical sciences',
        '24' => 'China',
        '25' => 'MedComm',
        '26' => 'GTA',
        '27' => 'Medicine Editing',
        '28' => 'Scholarly Communications',
        '29' => 'PSS',
        '30' => 'CRM',
        '31' => 'Med Comm',
        '32' => 'CDD Japan Retail',
        '33' => 'Procurement',
        '34' => 'CAJA',
        '35' => 'Client delight department',
        '36' => 'Med',
        '37' => 'PJS - Editage Academia',
        '38' => 'Software Development',
        '39' => 'Publication Support',
        '40' => 'Branding',
        '41' => 'Accounts',
        '42' => 'Life Science',
        '43' => 'Admin',
        '44' => 'Korea Retail',
        '45' => 'Medical Writing',
        '46' => 'Process Improvement',
        '47' => 'IT Infra',
        '48' => 'Delivery Academia',
        '49' => 'SKM',
        '50' => 'Life Sciences',
        '51' => 'CS',
        '52' => 'PMO',
    ],

    /**
     |-------------------------------------------------------------------------
     | Pagination
     |-------------------------------------------------------------------------
     */
    'pagination' => 10,

    /**
    |-------------------------------------------------------------------------
    | Default
    |-------------------------------------------------------------------------
     */
];